(ns lots-of-links.core
  (:require
    [clojure.data.json :as json]
    [clojure.string :as string]
    [java-time :as java-time]
    [ring.middleware.params :refer :all]
    [ring.util.response :as ring-util]
    [compojure.core :refer :all]
    [compojure.route :as route]
  )
)

(defn safe-string-ends-with?
  "Given a string and suffix,
  true if the string ends with the suffix,
  false otherwise"
  [target-suffix the-string]
  (if
    (nil? the-string)
    false
    (.endsWith the-string target-suffix)))

(defn semicolon-delimited-header-value-contains?
  "true iff target-group is among values in semicolon-delimited-header-value"
  [target-group semicolon-delimited-header-value]
  (if (nil? semicolon-delimited-header-value)
    false
    (contains?
      (set (string/split semicolon-delimited-header-value #";"))
      target-group
    )
  )
)

(defn semicolon-delimited-header-value-contains-any?
  "true iff set target-groups contains at least one group in the semicolon-delimited-header-value"
  [target-groups semicolon-delimited-header-value]
  (if (nil? semicolon-delimited-header-value)
    false
    (not
      (empty?
        (clojure.set/intersection
          (set (string/split semicolon-delimited-header-value #";"))
          target-groups
        )
      )
    )
  )
)

(defn before?
  "True iff the current local-date-time is before the given local-date-time"
  [some-local-date-time]
  (.isBefore (java-time/local-date-time) some-local-date-time)
)

(defn after?
  "True iff the current local-date-time is after the given local-date-time"
  [some-local-date-time]
  (.isAfter (java-time/local-date-time) some-local-date-time)
)


(def uw-madison-online-website-link
  {
    :title "Online degree website"
    :href "https://www.online.wisc.edu"
    :icon "bookmark"
    :target "_blank"
  }
)

(def online-undergrad-orientation-link
  {
    :title "Orientation"
    :href "https://canvas.wisc.edu"
    :icon "bookmark_border"
    :target "_blank"
  }
)

(def online-student-success-link
  {
    :title "Online student success"
    :href "https://oss.wisc.edu/"
    :icon "people"
    :target "_blank"
  }
)

(def soar-link
  {
    :title "SOAR"
    :href "https://oss.wisc.edu/soar"
    :icon "rocket_launch"
    :target "_blank"
  }
)

(def virtual-student-community-link
  {
    :title "Virtual student community"
    :href "https://canvas.wisc.edu/courses/221973"
    :icon "chat"
    :target "_blank"
  }
)

(def libraries-link
  {
    :title "Libraries"
    :href "https://www.library.wisc.edu"
    :icon "local_library"
    :target "_blank"
  }
)

(defn online-degree-resources
  "Returns vector of maps representing list-of-links links suitable for the viewing user given their ismemeberof header value"
  [is-member-of-header-value]
  (filter some? ;; filter out the nils from the ifs that evaluate false
    [
      (if
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          is-member-of-header-value
        )
        uw-madison-online-website-link
      )
      (if
        (or
          (semicolon-delimited-header-value-contains-any?
          #{
            "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
            "uw:domain:my.wisc.edu:audiences:uw-madison_online_enrolled_student"
          }
            is-member-of-header-value
          )
        )
        online-student-success-link
      )
      (if
        (or
          (semicolon-delimited-header-value-contains-any?
          #{
            "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
            "uw:domain:my.wisc.edu:audiences:uw-madison_online_enrolled_student"
          }
            is-member-of-header-value
          )
        )
        soar-link
      )
      (if
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_enrolled_student"
          is-member-of-header-value
        )
        virtual-student-community-link
      )
      (if
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_enrolled_student"
          is-member-of-header-value
        )
        libraries-link
      )
    ]
  )
)

(def myvote-link
  {
    :title "Register or check status"
    :href "https://myvote.wi.gov/"
    :icon "person"
    :target "_blank"
  }
)

(def preview-ballot-link
  {
    :title "Preview ballot"
    :href "https://myvote.wi.gov/previewmyballot"
    :icon "list"
    :target "_blank"
  }
)

(def voter-info-link
  {
    :title "Voter info"
    :href "https://vote.wisc.edu/"
    :icon "info_outline"
    :target "_blank"
  }
)

(def student-voter-id-link
  {
    :title "Voter ID Card"
    :href "https://voterid.wisc.edu/"
    :icon "badge"
    :target "_blank"
  }
)

(defn voting
  "Returns vector of maps representing list-of-links links suitable for the viewing user given their ismemberof header value"
  ;; Current implementation: everyone gets the first 3 links;
  ;; "students" get the fourth link, to the student voter ID.
  ;;
  [is-member-of-header-value]
  (filter some? ;; filter out the nils from the ifs that evaluate false
    [
      myvote-link
      preview-ballot-link
      voter-info-link
      (if
        (or
          (semicolon-delimited-header-value-contains-any?
            #{
              "uw:domain:my.wisc.edu:audiences:undergraduate_students_currently_enrolled"
              "uw:domain:my.wisc.edu:audiences:graduate_students_currently_enrolled"
              "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
              "uw:domain:my.wisc.edu:audiences:uw-madison_online_enrolled_student"
            }
            is-member-of-header-value
          )
        )
        student-voter-id-link
      )
    ]
  )
)

(def tes-login-link
  {
    :title "Login"
    :href "https://tes.collegesource.com/TES_login.aspx"
    :icon "login"
    :target "_blank"
  }
)

(def tes-vendor-collegesource-support-link
  {
    :title "Support"
    :href "https://support.collegesource.com/TES"
    :icon "support"
    :target "_blank"
  }
)

(def tes-madison-specific-learn-more-link
  {
    :title "Learn more"
    :href "https://kb.wisc.edu/registrar/109406"
    :icon "help_outline"
    :target "_blank"
  }
)

(defn tes
  "Returns vector of maps representing list-of-links links suitable for the viewing user given their ismemberof header value"
  ;; Current implementation:
  ;; everyone (who sees the widget) gets the first 2 links,
  ;; to the SaaS application and to the vendor's support for it.
  ;; Only myuw_transferology_access, myuw_transferology_lab_access
  ;; (that is, Madison transferology users) get the 3rd link,
  ;; to a Madison-specific KB article.
  ;;
  [is-member-of-header-value eppn-value]
  (filter some? ;; filter out the nils from the ifs that evaluate false
    [

      tes-login-link


      (if ; if the user is in one of the Manifest groups or is a Madison user
        (or ; include tes-madison-specific-learn-more-link
          (safe-string-ends-with? "wisc.edu" eppn-value)
          (semicolon-delimited-header-value-contains-any?
            #{
              "uw:domain:my.wisc.edu:audiences:myuw_transferology_lab_access"
              "uw:domain:my.wisc.edu:audiences:myuw_transferology_access"
            }
            is-member-of-header-value
          )
        )
        ;; Madison users get the Madison-specific link
        tes-madison-specific-learn-more-link
        ;; non-Madison users get the vendor-general link
        ;; Madison users don't need this link because the Madison-specific KB
        ;; page links to this vendor support website.
        tes-vendor-collegesource-support-link
      )
    ]
  )
)


;; Memoization is a technique for caching in-memory the responses.
;; Effectiveness hinges on the input to these functions is ismemberof headers
;; and there's just not that much variability in ismemberof headers.
;; With memoization if a function call presents with the same ismemberof headers
;; that have been seen before, responds with the cached function result.

(def memoized-online-degree-resources
  (memoize online-degree-resources)
)

(def memoized-voting
  (memoize voting)
)

(defn online-degree-resources-handler
  "JSON HTTP service returning the list-of-links JSON for online-degree-resources widget."
  [request]
  (let
    [is-member-of-header-value ((:headers request) "ismemberof")]
    {
      :status 200
      :headers {"Content-Type" "application/json"}
      :body (str
        (json/write-str
          {
            :content
            {
              :links (memoized-online-degree-resources is-member-of-header-value)
            }
          }
        )
      )
    }
  )
)

(defn voting-handler
  "JSON HTTP service returning the list-of-links JSON for voting widget."
  [request]
  (let
    [is-member-of-header-value ((:headers request) "ismemberof")]
    {
      :status 200
      :headers {"Content-Type" "application/json"}
      :body (str
        (json/write-str
          {
            :content
            {
              :links (memoized-voting is-member-of-header-value)
            }
          }
        )
      )
    }
  )
)

(defn tes-handler
  "JSON HTTP service returning the list-of-links JSON for TES widget."
  [request]
  (let
    [
      is-member-of-header-value ((:headers request) "ismemberof")
      eppn-value ((:headers request) "eppn")
    ]
    {
      :status 200
      :headers {"Content-Type" "application/json"}
      :body (str
        (json/write-str
          {
            :content
            {
              :links (tes is-member-of-header-value eppn-value)
            }
          }
        )
      )
    }
  )
)

(def uw-stout-ferpa-training-new-employees
  {
    :title "New hire FERPA"
    :href "https://go.wisc.edu/5g1uh9" ;; https://uws-td.instructure.com/enroll/WM43H9
    :icon "policy"
    :target "_blank"
  }
)

(def uw-stout-ferpa-refresher-training-2023
  {
    :title "FERPA Refresher 2023"
    :href "https://go.wisc.edu/dn7dkl" ;; https://uws-td.instructure.com/enroll/4N63TE
    :icon "policy"
    :target "_blank"
  }
)

(def uw-stout-employee-training-help-resources
  {
    :title "Help resources"
    :href "https://liveuwstout.sharepoint.com/sites/2022/Human-Resources/SitePages/Mandatory-Employee-Training.aspx"
    :icon "help"
    :target "_blank"
  }
)

(def uw-stout-employee-training-calendar
  {
    :title "Training calendar"
    :href "https://reg.abcsignup.com/view/view_month.aspx?as=12&wp=48&aid=UWS"
    :icon "calendar_today"
    :target "_blank"
  }
)

(defn uw-stout-employee-training
  "Returns vector of maps representing list-of-links links suitable for the viewing user given."
  []
  (filter some? ;; filter out the nils from the ifs that evaluate false
    [
      uw-stout-ferpa-training-new-employees
      (if
        (and
          (after? (java-time/local-date-time 2023 1 31 0 1))
          (before? (java-time/local-date-time 2023 3 10 23 59))
        )
        uw-stout-ferpa-refresher-training-2023
      )
      uw-stout-employee-training-help-resources
      uw-stout-employee-training-calendar
    ]
  )
)

(defn uw-stout-employee-training-handler
  "JSON HTTP service returning the list-of-links JSON for uw-stout-employee-training."
  [request]
  {
    :status 200
    :headers {"Content-Type" "application/json"}
    :body (str
      (json/write-str
        {
          :content
          {
            :links (uw-stout-employee-training)}}))})

(def html-scripts
  (str
    "<script type='module' src='https://unpkg.com/@myuw-web-components/myuw-app-styles@latest?module'></script>"
    "<script type='module' src='https://unpkg.com/@myuw-web-components/myuw-app-bar@latest?module'></script>"
  )
)

(def html-app-bar
  (str
    "<myuw-app-bar
      theme-name='MyUW'
      app-name='lots-of-links'
      app-url='/lots-of-links'>
    </myuw-app-bar>"
  )
)

(defn app-dir-localhost-stub-handler
  "Stub handling localhost requests that on tiers would have been handled by uPortal-home"
  [id]
  {
    :status 200
    :headers {"Content-Type" "text/html"}
    :body (str
      "<html>"
        "<head>" html-scripts "</head>"
        "<body>"
        html-app-bar
        "<h1>MyUW app directory localhost placeholder for " id " </h1>"
          "<p>When lots-of-links is deployed to MyUW tiers, this path would be the uPortal-home app directory entry for the list-of-links widget.</p>"
          "<p>On localhost uPortal-home is not present, so lots-of-links shows this placeholder page instead.</p>"
          "<p>You could view the app directory entry for the content on "
          "<a href='https://predev.my.wisc.edu/web/apps/details/" id "'>predev</a>, "
          "<a href='https://test.my.wisc.edu/web/apps/details/" id "'>test</a>, "
          "<a href='https://qa.my.wisc.edu/web/apps/details/" id "'>qa</a>, or "
          "<a href='https://my.wisc.edu/web/apps/details/" id "'>production my.wisc.edu</a>."
          "</p>"
        "</body>"
      "</html>"
    )
  }
)

(defn index-handler
  "HTML HTTP service responding with an index page listing the valid identifiers."
  [request]
  {
    :status 200
    :headers {"Content-Type" "text/html"}
    :body (str
      "<html>"
        "<head>" html-scripts "</head>"
        "<body>"
          html-app-bar
          "<h1>lots-of-links: a MyUW microservice for dynamic list-of-links JSON</h1>"
          "<p>These are the list-of-links dynamic JSON lots-of-links offers:</p>"
          "<ul>"
            "<li>"
              "online-degree-resources "
              "(<a href='./online-degree-resources'>JSON</a>, "
              "<a href='/web/apps/details/online-degree-resources'>UW-Madison Online Degree Resources in the app directory</a>)"
            "</li>"
            "<li>"
              "voting "
              "(<a href='./voting'>JSON</a>, "
              "<a href='/web/apps/details/voting'>Voting in the app directory</a>)"
            "</li>"
            "<li>"
              "TES "
              "(<a href='./tes'>JSON</a>, "
              "<a href='/web/apps/details/tes'>TES (Transfer Evaluation System) in the app directory</a>)"
            "</li>"
            "<li>"
              "UW-Stout employee training"
              "(<a href='./uw-stout-employee-training'>JSON</a>, "
              "<a href='/web/apps/details/uw-stout-training'>UW-Stout employee training in the app directory</a>)"
            "</li>"
          "</ul>"
        "</body>"
      "</html>"
    )
  }
)

(defroutes app
  (GET "/" [] index-handler)
  ;; since there are only a few, hard code routes.
  ;; for many more routes, id as data "/:id"
  (GET "/online-degree-resources" [] online-degree-resources-handler)
  (GET "/voting" [] voting-handler)
  (GET "/tes" [] tes-handler)
  (GET "/uw-stout-employee-training" [] uw-stout-employee-training-handler)
  ;; Simulate uPortal-home app directory on localhost
  (GET "/web/apps/details/:id" [id] (app-dir-localhost-stub-handler id))
  ;; Simulate that deployed to /lots-of-links when root context on localhost
  (GET "/lots-of-links" [] (ring-util/redirect "/"))
  (route/not-found "<h1>Page not found</h1>"))
