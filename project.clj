(defproject edu.wisc.my.app/lots-of-links "2.4.1-SNAPSHOT"
  :description "A back-end for MyUW list-of-links widgets"
  :url "https://git.doit.wisc.edu/myuw/lots-of-links"
  :scm {
    :name "git"
    :url "https://git.doit.wisc.edu/myuw/lots-of-links"
  }
  :license {
    :name "Apache License, Version 2.0"
    :url "https://www.apache.org/licenses/LICENSE-2.0.html"
    :distribution :repo
  }
  :javac-options ["-target" "1.8"]
  :dependencies [
    [clojure.java-time "0.3.2"]
    [org.clojure/clojure "1.10.1"]
    [org.clojure/data.json "1.0.0"] ; for converting to JSON
    [ring "1.8.2" :exclusions [commons-fileupload]] ; lightweight web framework
    [compojure "1.6.2" :exclusions [commons-fileupload]] ; a routing library for ring
  ]
  :plugins [
    [lein-ring "0.12.5"]
    [de.doctronic/lein-deploy-war "0.2.1"]
  ]
  :pedantic? :warn
  :ring {
    :handler lots-of-links.core/app
    :uberwar-name "lots-of-links.war"
  }
  :deploy-repositories [
    ["snapshots"
     "https://artifacts.doit.wisc.edu/artifactory/myuw-maven-local-snapshots"]
    ["releases"
     "https://artifacts.doit.wisc.edu/artifactory/myuw-maven-local-releases"]
  ]
)

