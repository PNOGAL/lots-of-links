(ns lots-of-links.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as string]
            [lots-of-links.core :refer :all]))

(deftest before?-test
  (testing "Given a date clearly in the past, returns false."
    (is (false?
      (before? (java-time/local-date-time 2005 9 1 11 22)))))
  (testing "Given a date clearly in the future, returns true."
    (is (true?
      (before? (java-time/local-date-time 2900 8 17 4 30))
    ))))

(deftest after?-test
  (testing "Given a date clearly in the past, returns true."
    (is (true?
      (after? (java-time/local-date-time 2005 9 1 11 22)))))
  (testing "Given a date clearly in the future, returns false."
    (is (false?
      (after? (java-time/local-date-time 2900 8 17 4 30))))))

(deftest semicolon-delimited-header-value-contains-test
  (testing "false when header value is empty string"
    (is
      (=
        false
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          ""
        )
      )
    )
  )
  (testing "false when header value is a different group"
    (is
      (=
        false
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_applicant"
        )
      )
    )
  )
  (testing "true when header value is the target group"
    (is
      (=
        true
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
        )
      )
    )
  )
  (testing "true when header includes the target group first"
    (is
      (=
        true
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student;uw:domain:my.wisc.edu:audiences:uw-madison_online_applicant"
        )
      )
    )
  )
  (testing "true when header includes the target group last without terminal semicolon"
    (is
      (=
        true
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_applicant;uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
        )
      )
    )
  )
  (testing "true when header includes the target group last with terminal semicolon"
    (is
      (=
        true
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_applicant;uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student;"
        )
      )
    )
  )
  (testing "false when header value is nil"
    (is
      (=
        false
        (semicolon-delimited-header-value-contains?
          "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student"
          nil
        )
      )
    )
  )
)

(deftest semicolon-delimited-header-value-contains-any?-test
  (testing "false when header value is empty string"
    (is
      (=
        false
        (semicolon-delimited-header-value-contains-any?
          #{"somegroup"}
          ""))))
  (testing "false when header contains no sought group"
    (is
      (=
        false
        (semicolon-delimited-header-value-contains-any?
          #{ "sought-group" "also-sought-group" }
          "agroup;anothergroup;but-none-of-the-sought-groups;"))))
  (testing "true when header contains a sought group"
    (is
      (=
        true
        (semicolon-delimited-header-value-contains-any?
          #{ "sought-group" "also-sought-group"}
          "agroup;sought-group;another-group;"))))
)

(deftest online-degree-resources-test
  (testing "Admitted UW-Madison online students get the website, student success, and SOAR links"
    (is
      (=
        [uw-madison-online-website-link online-student-success-link soar-link]
        (online-degree-resources "uw:domain:my.wisc.edu:audiences:uw-madison_online_admitted_student")
      )
    )
  )
  (testing "Enrolled get online student success, soar, communities, and libraries links"
    (is
      (=
        [online-student-success-link soar-link virtual-student-community-link libraries-link]
        (online-degree-resources "uw:domain:my.wisc.edu:audiences:uw-madison_online_enrolled_student")
      )
    )
  )

  (deftest tes-test
    (testing "Madison eppns get Madison-specific help link"
      (=
        [tes-login-link tes-madison-specific-learn-more-link]
        (tes "no-manifest-group" "bbadger@wisc.edu")
      )
    )
    (testing "Non-Madison eppns get generic help link"
      (=
        [tes-login-link tes-vendor-collegesource-support-link]
        (tes "no-manifest-group" "someone@uwrf.edu")
      )
    )
  )
)
