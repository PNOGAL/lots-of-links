# Change log

## Next

Now building 2.4.1-SNAPSHOT.

## 2.4.0 - 2023-01-19

+ Tee up 2023 UW-Stout FERPA refresher training.

## 2.3.1 - 2022-10-24

+ Online Degree Resources link changes

## 2.2.1 - 2022-02-08

+ Shorten labels on UW-Stout FERPA trainings to no longer truncate

## 2.2.0 - 2022-01-20

+ add `/uw-stout-employee-training` path
+ add `before?` and `after?` chronology predicate functions

## 2.1.2 - 2021-12-16

+ Show Madison users
  the Madison-specific learn more link in the /tes list-of-links;
  show the vendor-general support link only to non-Madison users.

## 2.1.1 - 2021-12-09

(This release did not include intended changes and is functionally equivalent to 2.1.0).

## 2.1.0 - 2021-04-29

+ Added `/tes` path for use as back-end to Transfer Evaluation System widget.

## 2.0.0 - 2021-04-01

BREAKING CHANGE: `/` now renders an index page rather than UW-Madison Online JSON.

+ Added index page listing the available list-of-links JSON identifiers.
+ Added localhost placeholder for the uPortal-home app directory, so that the
  index page hyperlinks work whether on localhost or as deployed to MyUW tiers.

## 1.1.0 - 2021-02-09

+ Added `/voting` path for use as back-end to Voting widget.
+ Added `/online-degree-resources` path
  for continued use by UW-Madison Online widget.

DEPRECATED: `/` providing the UW-Madison Online JSON is deprecated.
`/` may change in a future release.

## 1.0.3 - 2020-03-30

+ Removed Placement Testing ALEKS link ( [MYUWADMIN-968][] )

## 1.0.2 - 2020-03-12

+ Update link labels for UW-Madison Online widget ( [MYUWADMIN-943][] ):
  "Libraries" rather than "UW Madison Libraries",
  "Online degree website" rather than "UW-Madison Online website",
  "Orientation" rather than "Online undergraduate orientation"

## 1.0.1 - 2020-03-09

+ Fix JSON response to match that expected by dynamic list-of-links widgets.

## 1.0.0 - 2020-03-09

Initial release, for use as back-end to Online Undergraduate Degree widget.

[MYUWADMIN-943]: https://jira.doit.wisc.edu/jira/browse/MYUWADMIN-943
[MYUWADMIN-968]: https://jira.doit.wisc.edu/jira/browse/MYUWADMIN-968
