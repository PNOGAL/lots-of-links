#!/bin/bash

# Usage
# ./deploy [snapshot|release] VERSION
#
# Examples
#
# ./deploy snapshot 3.0.0-SNAPSHOT
#
# ./deploy release 3.1.0

#!/bin/bash

VERSION=$2

clean() {
echo "removing war files from ./target/"
rm ./target/*.war
echo "removing jar files from ./target/"
rm ./target/*.jar
echo "removing compiled class files from ./target/classes/"
rm -r ./target/classes/*
}

war() {
lein ring uberwar
}

deploy-snapshot() {
mvn deploy:deploy-file -DrepositoryId=artifacts.doit-myuw-public-snapshots -Durl=https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots  -Dfile=target/lots-of-links.war -DgroupId=edu.wisc.my.app -DartifactId=lots-of-links -Dversion=$VERSION -DgeneratePom=true -DgeneratePom.description="Back-end for MyUW list-of-links widgets"
}

deploy-release() {
mvn deploy:deploy-file -DrepositoryId=artifacts.doit-myuw-public-releases -Durl=https://artifacts.doit.wisc.edu/artifactory/myuw-public-releases  -Dfile=target/lots-of-links.war -DgroupId=edu.wisc.my.app -DartifactId=lots-of-links -Dversion=$VERSION -DgeneratePom=true -DgeneratePom.description="Back-end for MyUW list-of-links widgets"
}

case "$1" in
snapshot)
    clean
    war
    deploy-snapshot
    ;;
release)
    clean
    war
    deploy-release
    ;;
*)
    echo $"Usage: $0 {snapshot|release} VERSION"
    echo ""
    echo "For example"
    echo $"$0 snapshot 3.0.0-SNAPSHOT"
    echo ""
    echo "or"
    echo ""
    echo $"$0 release 3.1.0"
    echo ""
    echo "VERSION must match that in project.clj"
    exit 2
    ;;
esac

exit $?
