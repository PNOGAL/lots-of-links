# TODO: Ideas on what to do next in lots-of-links

+ Set cache headers
+ Add healthcheck and diagnostics paths.
+ Add UI alongside the JSON API
+ Try compiling to a container and running in e.g. Amazon Lightsail.
