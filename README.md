# lots-of-links

lots-of-links is a back-end for MyUW list-of-links widgets.

Currently it provides the list-of-links JSON supporting the

+ Transfer Evaluation System
+ UW-Madison Online
* UW-Stout Employee Training
+ Voting

list-of-links widgets.

## Kicking the tires

### Exercising as deployed

Anyone can exercise the `lots-of-links` service as deployed at
<https://public.my.wisc.edu/lots-of-links/>.

This isn't very interesting as it will always reply with the same responses
because `public.my.wisc.edu` is not Shibbolized so the `lots-of-links` service
there cannot personalize the links based on your identity.
You don't have an `ismemberof` header on public.my.

UW-Madison users can exercise the service as logged in via NetID at

<https://my.wisc.edu/lots-of-links/>

but for most users it still won't be very interesting because you won't be a
member of the groups that would cause it to give you different links.

### Running locally

`lein ring server`

will spin it up running on localhost:3000.

To exercise it in interesting ways you'll need to set the `ismemberof` header,
e.g. via a browser plugin.

### Tests

`lein test`

## What is it

`lots-of-links` is a Clojure web application implementing the back-end of
list-of-links widgets. It responds with [JSON to drive list-of-links widgets][]
in the uPortal-app-framework.

## Releasing, deploying, and migrating

### Releasing

Generate a `.war` file.

```shell
$ lein ring uberwar

...

Created /Users/apetro/code/myuw_gitdoit/lots-of-links/target/lots-of-links.war
```

### Deploying

Here "deploy" is referencing deployment to the artifact repository.

#### To deploy a SNAPSHOT

(This requires first creating the .war using `lein ring uberwar` above.)

The provided `deploy.sh` script will deploy the war to artifacts.doit.

```sh
./deploy.sh snapshot VERSION
```

for example,

```sh
$ ./deploy.sh snapshot 2.2.0-SNAPSHOT
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------< edu.wisc.my.app:lots-of-links >--------------------
[INFO] Building lots-of-links 1.1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-deploy-plugin:2.7:deploy-file (default-cli) @ lots-of-links ---
Downloading from artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/maven-metadata.xml
Downloaded from artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/maven-metadata.xml (766 B at 3.0 kB/s)
Uploading to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/lots-of-links-2.2.0-20210429.161458-2.war
Uploaded to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/lots-of-links-2.2.0-20210429.161458-2.war (7.0 MB at 6.8 MB/s)
Uploading to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/lots-of-links-2.2.0-20210429.161458-2.pom
Uploaded to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/lots-of-links-2.2.0-20210429.161458-2.pom (508 B at 3.0 kB/s)
Downloading from artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/maven-metadata.xml
Downloaded from artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/maven-metadata.xml (609 B at 7.2 kB/s)
Uploading to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/maven-metadata.xml
Uploaded to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/2.2.0-SNAPSHOT/maven-metadata.xml (778 B at 5.5 kB/s)
Uploading to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/maven-metadata.xml
Uploaded to artifacts.doit-myuw-public-snapshots: https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots/edu/wisc/my/app/lots-of-links/maven-metadata.xml (564 B at 3.7 kB/s)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2.369 s
[INFO] Finished at: 2021-04-29T11:15:00-05:00
[INFO] ------------------------------------------------------------------------
```

You could also do manually what the script does:

```shell
mvn deploy:deploy-file
  -DrepositoryId=SOME_ID \
  -Durl=https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots \
  -Dfile=target/lots-of-links.war \
  -DgroupId=edu.wisc.my.app \
  -DartifactId=lots-of-links \
  -Dversion=VERSION \
  -DgeneratePom=true \
  -DgeneratePom.description="Back-end for MyUW list-of-links widgets"
```

SOME_ID should be whatever ID you have for `myuw-public-snapshots` in your
`~/.m2/settings.xml`; mine is `artifacts.doit-myuw-public-snapshots`.

```xml
<server>
     <id>artifacts.doit-myuw-public-snapshots</id>
     <username>apetro</username>
     <password>REDACTED</password>
</server>
```

VERSION should match that in `project.clj`, e.g. `0.1.0-SNAPSHOT`.

#### To deploy a release

The included `deploy.sh` shell script can also deploy releases.

```sh
./deploy.sh release VERSION
```

VERSION should match that in `project.clj`.

You can also do what the script does manually, by
switching `repositoryId`, `url`, and `version` above to the appropriate values.

+ `repositoryId`: as configured in your local Maven `settings.xml`;
  mine is `artifacts.doit-myuw-public-releases`
+ `url`: `https://artifacts.doit.wisc.edu/artifactory/myuw-public-releases`
+ `version`: the appropriate not-SNAPSHOT version number; e.g. `1.0.0`

There are other ways to deploy this (as an executable .jar, or even in its own
Docker container!), but this `.war` via artifact repository is the one most
plausible in the status quo MyUW service.

### Migrating

Migrating is the process whereby a release that was deployed to an artifact
repository is made available on a MyUW server.

Add edu.wisc.my.app:list-of-links:war of the appropriate version to the
ear file, or update its version there.

[JSON to drive list-of-links widgets]: https://uportal-project.github.io/uportal-app-framework/make-a-widget.html#sourcing-list-of-links-content-from-a-url
